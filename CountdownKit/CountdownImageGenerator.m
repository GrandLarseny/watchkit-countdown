//
//  CountdownImageGenerator.m
//  Countdown
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import "CountdownImageGenerator.h"
#import "CountdownStyleKit.h"

@implementation CountdownImageGenerator

static NSInteger imageHeight;

+ (void)initialize
{
	imageHeight = 108;
}

+ (NSArray *)imagesForSegments:(NSArray	*)segmentValues withTotalDuration:(NSInteger)duration atFramesPerSecond:(NSInteger)fps
{
	return [self imagesForSegments:segmentValues startingAtTime:0 withTotalDuration:duration atFramesPerSecond:fps];
}

+ (NSArray *)imagesForSegments:(NSArray *)segmentValues startingAtTime:(NSTimeInterval)start withTotalDuration:(NSInteger)duration atFramesPerSecond:(NSInteger)fps
{
	NSMutableArray *allImages = [NSMutableArray new];
	
	NSInteger segment1Duration = [[segmentValues objectAtIndex:0] integerValue];
	NSInteger segment2Duration = [[segmentValues objectAtIndex:1] integerValue];
	
	NSInteger seconds = floor(start);
	// The frame is the portion of the start remainder in terms of FPS
	NSInteger startFrame = (start - seconds) * fps;

	// Complete the first, partial run of frames
	for (NSInteger frame = startFrame; frame < fps; frame++) {
		CGFloat currentDuration = seconds + ((CGFloat)frame / fps);
		[allImages addObject:[CountdownStyleKit imageOfSegmentedTimerWithTotalHeight:imageHeight totalProgress:currentDuration segment1Duration:segment1Duration segment2Duration:segment2Duration]];
	}
	
	for (NSInteger second = seconds + 1; second < duration; second++) {
		for (NSInteger frame = 0; frame < fps; frame++) {
			CGFloat currentDuration = second + ((CGFloat)frame / fps);
			[allImages addObject:[CountdownStyleKit imageOfSegmentedTimerWithTotalHeight:imageHeight totalProgress:currentDuration segment1Duration:segment1Duration segment2Duration:segment2Duration]];
		}
	}
	
	[allImages addObject:[CountdownStyleKit imageOfSegmentedTimerWithTotalHeight:imageHeight totalProgress:segment1Duration + segment2Duration segment1Duration:segment1Duration segment2Duration:segment2Duration]];
	
	return allImages;
}

+ (UIImage *)imageForSegments:(NSArray *)segmentValues withRemainingDuration:(CGFloat)remaining;
{
	CGFloat segment1Duration = [[segmentValues objectAtIndex:0] integerValue];
	CGFloat segment2Duration = [[segmentValues objectAtIndex:1] integerValue];
	
	CGFloat currentProgress = (segment1Duration + segment2Duration) - remaining;

	return [CountdownStyleKit imageOfSegmentedTimerWithTotalHeight:imageHeight totalProgress:currentProgress segment1Duration:segment1Duration segment2Duration:segment2Duration];
}

+ (void)updateImageHeight:(NSInteger)height
{
	imageHeight = height;
}

@end
