//
//  CountdownKit.h
//  CountdownKit
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CountdownKit.
FOUNDATION_EXPORT double CountdownKitVersionNumber;

//! Project version string for CountdownKit.
FOUNDATION_EXPORT const unsigned char CountdownKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CountdownKit/PublicHeader.h>


