//
//  AppDelegate.m
//  Animation Generation
//
//  Created by Daniel Larsen on 1/28/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import "AppDelegate.h"
#import "CountdownStyleKit.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	[CountdownStyleKit initialize];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}

@end
