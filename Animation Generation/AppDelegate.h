//
//  AppDelegate.h
//  Animation Generation
//
//  Created by Daniel Larsen on 1/28/15.
//  Copyright (c) 2015 BottleRocket. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

