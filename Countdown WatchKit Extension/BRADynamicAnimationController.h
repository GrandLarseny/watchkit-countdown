//
//  InterfaceController.h
//  Countdown WatchKit Extension
//
//  Created by Daniel Larsen on 12/29/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface BRADynamicAnimationController : WKInterfaceController

@end
