//
//  ViewController.h
//  Countdown
//
//  Created by Daniel Larsen on 12/29/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CountdownValueConfiguration <NSObject>

@required

- (NSArray *)segmentValues;

@end

@interface SegmentConfiguration : UIViewController <CountdownValueConfiguration>


@end

