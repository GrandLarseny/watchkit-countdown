//
//  ViewController.m
//  Countdown
//
//  Created by Daniel Larsen on 12/29/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import "SegmentConfiguration.h"
#import "CountdownPreview.h"
#import "CountdownConstants.h"

@interface SegmentConfiguration ()

@property (weak, nonatomic) IBOutlet UILabel *segment1ValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *segment2ValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *fpsLabel;
@property (weak, nonatomic) IBOutlet UISlider *segment1Slider;
@property (weak, nonatomic) IBOutlet UISlider *segment2Slider;
@property (weak, nonatomic) IBOutlet UISlider *fpsSlider;
@property (weak, nonatomic) IBOutlet CountdownPreview *preview;
@property (weak, nonatomic) IBOutlet UIButton *startStopButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@end

@implementation SegmentConfiguration

- (void)viewDidLoad {
	[super viewDidLoad];

	self.segment1Slider.value = [self loadIntegerFromDefaults:CountdownExtensionSegment1ValueUserDefault withFallbackDefault:self.segment1Slider.value];
	self.segment2Slider.value = [self loadIntegerFromDefaults:CountdownExtensionSegment2ValueUserDefault withFallbackDefault:self.segment2Slider.value];
	self.fpsSlider.value = [self loadIntegerFromDefaults:CountdownExtensionFramesPerSecondUserDefault withFallbackDefault:self.fpsSlider.value];
	
	self.segment1ValueLabel.text = [NSString stringWithFormat:@"%li", (NSInteger)self.segment1Slider.value];
	self.segment2ValueLabel.text = [NSString stringWithFormat:@"%li", (NSInteger)self.segment2Slider.value];
	self.fpsLabel.text = [NSString stringWithFormat:@"%li", (NSInteger)self.fpsSlider.value];

	self.preview.segment1Value = self.segment1Slider.value;
	self.preview.segment2Value = self.segment2Slider.value;
	self.preview.previewFramesPerSecond = self.fpsSlider.value;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)segment1Changed:(id)sender {
	self.segment1ValueLabel.text = [NSString stringWithFormat:@"%li", (NSInteger)self.segment1Slider.value];
	self.preview.segment1Value = self.segment1Slider.value;
}

- (IBAction)segment2Changed:(id)sender {
	self.segment2ValueLabel.text = [NSString stringWithFormat:@"%li", (NSInteger)self.segment2Slider.value];
	self.preview.segment2Value = self.segment2Slider.value;
}

- (IBAction)fpsValueChanged:(id)sender
{
	self.fpsLabel.text = [NSString stringWithFormat:@"%li", (NSInteger)self.fpsSlider.value];
	self.preview.previewFramesPerSecond = (NSInteger)self.fpsSlider.value;
}

#pragma mark - CountdownValueConfiguration

- (NSArray *)segmentValues
{
	return @[[NSNumber numberWithInteger:self.segment1Slider.value], [NSNumber numberWithInteger:self.segment2Slider.value]];
}

#pragma mark - Actions

- (IBAction)startStopPressed:(id)sender
{
	if ([self.startStopButton.currentTitle isEqualToString:@"Start"]) {
		[self.startStopButton setTitle:@"Stop" forState:UIControlStateNormal];
	} else {
		[self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
	}
}

- (IBAction)resetPressed:(id)sender
{
	[self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
}

#pragma mark - Private

- (NSInteger)loadIntegerFromDefaults:(NSString *)key withFallbackDefault:(float)defaultValue
{
	NSNumber *integerNumber = [[NSUserDefaults standardUserDefaults] objectForKey:key];
	if (integerNumber) {
		return [integerNumber integerValue];
	}
	return defaultValue;
}

@end
