//
//  AppDelegate.h
//  Countdown
//
//  Created by Daniel Larsen on 12/29/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

