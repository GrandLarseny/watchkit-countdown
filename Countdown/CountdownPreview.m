//
//  CountdownPreview.m
//  Countdown
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <malloc/malloc.h>
#import "CountdownPreview.h"
#import "CountdownStyleKit.h"
#import "CountdownImageGenerator.h"
#import "CountdownConstants.h"

@interface CountdownPreview ()

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, assign) NSTimeInterval remainingTime;
@property (nonatomic, strong) UIImageView *previewImage;
@property (nonatomic, readonly, assign) NSInteger totalTime;
@property (weak, nonatomic) IBOutlet UILabel *animationSize;

@end

@implementation CountdownPreview

#pragma mark - Lifecycle

- (void)awakeFromNib
{
	self.previewImage = [[UIImageView alloc] initWithFrame:self.bounds];
	self.previewImage.contentMode = UIViewContentModeTop;
	[self addSubview:self.previewImage];
}

#pragma mark - Accessors

- (void)setSegment1Value:(NSInteger)segment1Value
{
	_segment1Value = segment1Value;
	[self displayPreviewImage];
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:segment1Value] forKey:CountdownExtensionSegment1ValueUserDefault];
}

- (void)setSegment2Value:(NSInteger)segment2Value
{
	_segment2Value = segment2Value;
	[self displayPreviewImage];
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:segment2Value] forKey:CountdownExtensionSegment2ValueUserDefault];
}

- (void)setPreviewFramesPerSecond:(NSInteger)previewFramesPerSecond
{
	_previewFramesPerSecond = previewFramesPerSecond;
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:previewFramesPerSecond] forKey:CountdownExtensionFramesPerSecondUserDefault];
}

- (NSInteger)totalTime
{
	return self.segment1Value + self.segment2Value;
}

#pragma mark - Actions

- (IBAction)startStopAnimation:(id)sender
{
	if (self.previewImage.isAnimating) {
		self.remainingTime += [self.startTime timeIntervalSinceNow];
		[self.previewImage stopAnimating];
		NSArray *animationImages = [CountdownImageGenerator imagesForSegments:@[ [NSNumber numberWithInteger:self.segment1Value], [NSNumber numberWithInteger:self.segment2Value] ] startingAtTime:(self.totalTime - self.remainingTime) withTotalDuration:self.totalTime atFramesPerSecond:self.previewFramesPerSecond];
		[self.previewImage setAnimationImages:animationImages];
		[self.previewImage setAnimationDuration:self.remainingTime];
		[self.previewImage setImage:[CountdownImageGenerator imageForSegments:@[ [NSNumber numberWithInteger:self.segment1Value], [NSNumber numberWithInteger:self.segment2Value] ] withRemainingDuration:self.remainingTime]];
		[self updateAnimationImageSizes:animationImages];
		self.startTime = nil;
	} else {
		self.startTime = [NSDate date];
		if (self.remainingTime == 0) {
			self.remainingTime = self.totalTime;
			NSArray *animationImages = [CountdownImageGenerator imagesForSegments:@[ [NSNumber numberWithInteger:self.segment1Value], [NSNumber numberWithInteger:self.segment2Value] ] startingAtTime:(self.totalTime - self.remainingTime) withTotalDuration:self.totalTime atFramesPerSecond:self.previewFramesPerSecond];
			[self.previewImage setAnimationImages:animationImages];
			[self.previewImage setAnimationDuration:self.totalTime];
			[self.previewImage setAnimationRepeatCount:1];
			[self updateAnimationImageSizes:animationImages];
		}
		[self.previewImage startAnimating];
	}
}

- (IBAction)resetPreview:(id)sender
{
	self.startTime = nil;
	self.remainingTime = 0;
	self.animationSize.text = @"--";
	[self.previewImage stopAnimating];
	[self displayPreviewImage];
}

#pragma mark - Private

- (void)updateAnimationImageSizes:(NSArray *)animationImages
{
	__weak typeof(self) weakSelf = self;
	dispatch_async(dispatch_get_main_queue(), ^() {
			
		int totalSize = 0;
		for (id img in animationImages) {
			NSData *imageData = UIImagePNGRepresentation(img);
			totalSize += [imageData length];
			//totalSize += malloc_size((__bridge const void *) img);
		}
		weakSelf.animationSize.text = [NSString stringWithFormat:@"%@", [NSByteCountFormatter stringFromByteCount:totalSize countStyle:NSByteCountFormatterCountStyleMemory]];
	});
}

- (void)displayPreviewImage
{
	[self.previewImage setImage:[CountdownImageGenerator imageForSegments:@[ [NSNumber numberWithInteger:self.segment1Value], [NSNumber numberWithInteger:self.segment2Value] ]  withRemainingDuration:0]];
}

@end
