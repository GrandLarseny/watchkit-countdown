//
//  CountdownPreview.h
//  Countdown
//
//  Created by Daniel Larsen on 12/31/14.
//  Copyright (c) 2014 BottleRocket. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountdownPreview : UIView

@property (nonatomic, assign) NSInteger segment1Value;
@property (nonatomic, assign) NSInteger segment2Value;
@property (nonatomic, assign) NSInteger previewFramesPerSecond;

@end
